<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_coupon'                    => 'Ajouter ce coupon de réduction',

	// C
	'champ_actif_label'                      => 'Coupon activé',
	'champ_code_label'                       => 'Code',
	'champ_date_creation_label'              => 'Date de creation',
	'champ_date_validite_label'              => 'Date de fin de validité',
	'champ_id_commandes_detail_label'        => 'Généré par la commande',
	'champ_montant_label'                    => 'Montant',
	'champ_restriction_auteur_label'         => 'Restreint à l\'auteur n°',
	'champ_restriction_auteur_titre_label'   => 'Restreint à l\'auteur ',
	'champ_restriction_produit_label'        => 'Restreint aux produits n°',
	'champ_restriction_produit_explication'  => 'Valeurs séparées par une virgule ou un espace',
	'champ_restriction_produit_titre_label'  => 'Restreint aux produits',
	'champ_restriction_taxe_label'           => 'Restreint aux objets de la taxe',
	'champ_titre_label'                      => 'Titre',
	'champ_utilisation_multiple_label'       => 'Utilisation multiple',
	'champ_utilisation_multiple_explication' => 'Pas de restriction sur le nombre d\'utilisations du coupon',
	'confirmer_supprimer_coupon'             => 'Confirmez-vous la suppression de cet coupon de réduction ?',
	'coupon_genere'                          => 'Coupon généré',
	'coupon_utilise'                         => 'Coupon utilisé',
	'coupon_utilisable'                      => 'Coupon utilisable sur le site',

	// E
	'explication_code_label'                 => 'Laisser vide pour le générer aléatoirement',
	'erreur_code_deja_utilise'               => 'Ce code est déjà utilisé par un coupon',

	// I
	'icone_creer_coupon'                     => 'Créer un coupon de réduction',
	'icone_modifier_coupon'                  => 'Modifier ce coupon de réduction',
	'info_1_coupon'                          => 'Un coupon de réduction',
	'info_aucun_coupon'                      => 'Aucun coupon de réduction',
	'info_coupons_auteur'                    => 'Les coupons de réduction de cet auteur',
	'info_nb_coupons'                        => '@nb@ coupons de réduction',

	// M
	'montant_utilisable'                     => 'Montant restant à utilise',

	// R
	'retirer_lien_coupon'                    => 'Retirer ce coupon de réduction',
	'retirer_tous_liens_coupons'             => 'Retirer tous les coupons de réduction',

	// S
	'supprimer_coupon'                       => 'Supprimer cet coupon de réduction',

	// T
	'texte_ajouter_coupon'                   => 'Ajouter un coupon de réduction',
	'texte_changer_statut_coupon'            => 'Ce coupon de réduction est :',
	'texte_creer_associer_coupon'            => 'Créer et associer un coupon de réduction',
	'texte_definir_comme_traduction_coupon'  => 'Ce coupon de réduction est une traduction du coupon de réduction numéro :',
	'titre_coupon'                           => 'Coupon de réduction',
	'titre_coupons'                          => 'Coupons de réduction',
	'titre_coupons_rubrique'                 => 'Coupons de réduction de la rubrique',
	'titre_langue_coupon'                    => 'Langue de ce coupon de réduction',
	'titre_logo_coupon'                      => 'Logo de ce coupon de réduction',
	'titre_objets_lies_coupon'               => 'Liés à ce coupon de réduction',

	// U
	'utilise_dans_commandes'                 => 'Utilisé dans les commandes',
);
