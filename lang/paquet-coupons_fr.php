<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'coupons_description' => '',
	'coupons_nom'         => 'Coupons et bons d\'achat',
	'coupons_slogan'      => '',
);
